# Schedule Notifier Bot
## About

It's a helpful tool notificating students about their classes schedule. This telegram bot wakes up via CRON and sends a message about today's and tomorrow's schedule. You can manage a timetable in `variables.py` file. Also, you can specify days off in this file. Here is an example of `variables.py`:
```python
weekdays = ["monday", "tuesday", "wednesday", "thursday", "friday", "sunday", "saturday"]
timetable = [
    ["8:00", "9:30"],
    ["9:40", "11:10"],
    ["11:30", "13:00"],
    ["13:10", "14:40"],
    ["15:00", "16:30"],
    ["16:40", "18:10"],
    ["18:20", "19:50"],
    ["20:00", "21:30"]
]
days_off = ["01-05-2019"]
schedule = {
    "monday": {
        "first_number_class": 1,
        "classes": [
            {
                "always": {},
                "numerator": {},
                "denumerator": {
                    "name": "Subject 1",
                    "classroom": "",
                    "kind": "lecture"
                }
            },
            {
                "always": {
                    "name": "Subject 1",
                    "classroom": "131",
                    "kind": "practice"
                },
                "numerator": {},
                "denumerator": {}
            },
            {
                "always": {},
                "numerator": {
                    "name": "Subject 2",
                    "classroom": "",
                    "kind": "lecture"
                },
                "denumerator": {
                    "name": "Subject 2",
                    "classroom": "",
                    "kind": "practice"
                }
            }
        ]
    }
}
```
<!-- Note that `va.json` has a "non-coat classrooms" property. This is neccessary to remind students to leave their outerwear in a cloak-room, because on a specific day there will be classes in a computer classroom. -->

<!-- ## Screenshots

<img src="https://raw.githubusercontent.com/agvolkov5/schedule-notifier-bot/master/screenshot1.png" data-canonical-src="https://raw.githubusercontent.com/agvolkov5/schedule-notifier-bot/master/screenshot1.png" height="300" />
<img src="https://raw.githubusercontent.com/agvolkov5/schedule-notifier-bot/master/screenshot2.png" data-canonical-src="https://raw.githubusercontent.com/agvolkov5/schedule-notifier-bot/master/screenshot2.png" height="265" /> -->
