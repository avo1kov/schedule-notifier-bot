from variables import weekdays, timetable, days_off, schedule
from telegram_credentials import token, chat_id, test_chat_id
import requests, datetime, json

now = datetime.datetime.now() + datetime.timedelta(days=1)
weeknumber = now.isocalendar()[1]

def render_message(schedule, today=True, date=None):
    russian_weekdays = ["понедельник", "вторник", "среду", "четверг", "пятницу", "субботу", "воскресенье"]
    if today:
        message = "Сегодня:\n"
    else:
        if (now + datetime.timedelta(days=1) == date):
            message = "Завтра:\n"
        else:
            message = ("Во " if date.weekday() == 1 else "В ") + russian_weekdays[date.weekday()] + ":\n"
    for index, class_item in enumerate(schedule["classes"]):
        today_class_item = {}
        if "always" in class_item and class_item["always"] != {}:
            today_class_item = class_item["always"]          
        elif (not weeknumber % 2) and "numerator" in class_item and class_item["numerator"] != {}:
            today_class_item = class_item["numerator"]
        elif (weeknumber % 2) and "denumerator" in class_item and class_item["denumerator"] != {}:
            today_class_item = class_item["denumerator"]
        if today_class_item != {}:
            message += "_" + timetable[schedule["first_number_class"] + index - 1][0] + "_ "
            message += today_class_item["name"]
            if today_class_item["kind"] == "practice":
                message += " ⛏"
            elif today_class_item["kind"] == "lecture":
                message += " 🗣"
            if today_class_item["classroom"]:
                message += " _" + today_class_item["classroom"] + "_"
            message += "\n"
    if today:
        message += "\n"
        message += "Пары до " + timetable[schedule["first_number_class"] + index - 1][1] + "\n"
    return message

if now.strftime("%d-%m-%Y") not in days_off and weekdays[now.weekday()] in schedule:
    print(now)
    message = ""
    today_schedule = schedule[weekdays[now.weekday()]]
    message += render_message(today_schedule)
    message += "\n"

    next_date = now + datetime.timedelta(days=1)
    weeknumber = next_date.isocalendar()[1]
    while next_date.strftime("%d-%m-%Y") in days_off or weekdays[next_date.weekday()] not in schedule:        
        next_date += datetime.timedelta(days=1)
        weeknumber = next_date.isocalendar()[1]

    next_schedule = schedule[weekdays[next_date.weekday()]]
    message += render_message(next_schedule, today=False, date=next_date)
    response = requests.get('https://api.telegram.org/bot' + token + '/sendMessage?parse_mode=Markdown&chat_id=' + chat_id + "&text=" + message)
    got_response = json.loads(response.content)
    with open('response_from_tg.json', 'w') as response_file:
        json.dump(got_response, response_file)
    
    print(message)
else:
    print("Today is day off..")

