#!/usr/bin/env python
# coding: utf-8

# In[1]:


import requests
import json
import datetime
from telegram_credentials import token, chat_id, test_chat_id
import pytz

# In[2]:


timetable = [
    ["8:00", "9:30"],
    ["9:40", "11:10"],
    ["11:30", "13:00"],
    ["13:10", "14:40"],
    ["15:00", "16:30"],
    ["16:40", "18:10"],
    ["18:20", "19:50"],
    ["20:00", "21:30"]
]
rename_table = [
    ["Нечеткий анализ и моделирование", "Нечёткий анал"],
    ["Модели интеллектуальных систем", "Костенко"],
    ["Алгоритмы цифровой обработки мультимедиа", "Мультимедиа"],
    ["Модели поисковой оптимизации", "Поиск. оптим."],
    ["Программирование для мобильных платформ", "Java"],
    ["Разработка технической документации", "Тех. док."],
    ["Распределенные задачи и алгоритмы", "Распред. алг-мы"],
    ["Верификация программных систем", "Жуков"],
    ["Верификация программых систем", "Жуков"]
]
days_off = []


# In[3]:


choosen_chat_id = chat_id


# In[4]:


got_schedule = {}
friends_schedule = {}
schedule_was_changed = False
with open('friends_schedule.json', 'r') as schedule_file:
    friends_schedule = json.load(schedule_file)

try:
    data = requests.get('https://api.npoint.io/51288cb390c242f3e007')
    if data:
        got_schedule = json.loads(data.content)
        if friends_schedule != got_schedule:
            with open('friends_schedule.json', 'w') as schedule_file:
                json.dump(got_schedule, schedule_file)
            friends_schedule = got_schedule
            schedule_was_changed = True
            print("Got changes from friends API. Let's edit telegram message!")
        else:
            print("Changes not found. Nothing to edit.")
    else:
        print("Failed to connect to friends API. Schedule loaded from reserve copy.")
except:
    print("Unknown error in retrieving schedule from friends API. Schedule loaded from reserve copy.")
    pass


# In[5]:


pairs = friends_schedule['pairs']
pairs = list(filter(lambda pair: pair if pair['group'] == '46/1' else False, pairs))
pairs = sorted(pairs, key=lambda pair: pair['number'])


# In[6]:


def filter_for_pairs(pair, date):
    weeknumber = date.isocalendar()[1]
    if pair['day'] == date.weekday() + 1:
        if pair['even'] == 2:
            return pair
        if pair['even'] == 0 and not weeknumber % 2:
            return pair
        if pair['even'] == 1 and weeknumber % 2:
            return pair
    return False


# In[27]:


def render_message_by_friends(schedule, date=None, tomorrow=False):
    russian_weekdays = ["понедельник", "вторник", "среду", "четверг", "пятницу", "субботу", "воскресенье"]
    if date == None:
        message = "_Сегодня"
    else:
        if (datetime.datetime.now(pytz.timezone("Europe/Moscow")).replace(hour=0, minute=0, second=0, microsecond=0) + datetime.timedelta(days=2) == date.replace(hour=0, minute=0, second=0, microsecond=0)) or tomorrow:
            message = "_Завтра"
        else:
            message = ("_Во " if date.weekday() == 1 else "_В ") + russian_weekdays[date.weekday()]
    message += " с " + str(timetable[schedule[0]['number'] - 1][0]) + " до " + str(timetable[schedule[len(schedule)-1]['number'] - 1][1]) + ":_\n\n"
    for pair in schedule:
        message += "_" + timetable[pair['number'] - 1][0] + "_"
        pair_name = pair['name']
        for row in rename_table:
            pair_name = pair_name.replace(row[0], row[1])
        message += " " + pair_name
        message += " " + ["🗣", "⛏"][pair['type']]
        message += " _" + pair['studyroom'] + "_" if pair['studyroom'] else ""
        message += "\n"
        
    return message


# In[25]:


def render_all_message(purpose='new'):
    message = ""
    now = datetime.datetime.now(pytz.timezone("Europe/Moscow"))
    if now.hour == 0:
        now += datetime.timedelta(days=1)
    if purpose == 'edit':
        if datetime.datetime.now(pytz.timezone("Europe/Moscow")).hour != 0:
            now = datetime.datetime.now(pytz.timezone("Europe/Moscow"))
    if now.strftime("%d-%m-%Y") not in days_off and len(list(filter(lambda pair: filter_for_pairs(pair, now), pairs))):
#         print(now)
        message += render_message_by_friends(list(filter(lambda pair: filter_for_pairs(pair, now), pairs))) + "\n"

        next_date = now + datetime.timedelta(days=1)
        while next_date.strftime("%d-%m-%Y") in days_off or not len(list(filter(lambda pair: filter_for_pairs(pair, next_date), pairs))):        
            next_date += datetime.timedelta(days=1)

#         print(next_date)
        tomorrow = False
        if purpose == 'edit':
            if (now.replace(hour=0, minute=0, second=0, microsecond=0) + datetime.timedelta(days=1) == next_date.replace(hour=0, minute=0, second=0, microsecond=0)):
                tomorrow = True
        message += render_message_by_friends(list(filter(lambda pair: filter_for_pairs(pair, next_date), pairs)), date=next_date, tomorrow=tomorrow)
    else:
        print(now)
        print("Day off..")
    return message


# In[37]:


now = datetime.datetime.now(pytz.timezone("Europe/Moscow"))
with open('last_post.json', 'r') as last_post_file:
    last_post_info = json.load(last_post_file)
    print(last_post_info)
    print(last_post_info['date'], now.strftime("%d-%m-%Y"), now.hour)
    if last_post_info['date'] != now.strftime("%d-%m-%Y"):
        print("Not posted.")
        date = datetime.datetime.now(pytz.timezone("Europe/Moscow"))
        if now.hour == 0:
            date += datetime.timedelta(days=1)
        if date.strftime("%d-%m-%Y") not in days_off and len(list(filter(lambda pair: filter_for_pairs(pair, date), pairs))):
            print("Post!")
            message = render_all_message()
            print(message)
            response = requests.get('https://api.telegram.org/bot' + str(token) + '/sendMessage?parse_mode=Markdown&chat_id=' + str(choosen_chat_id) + "&text=" + str(message))
            response_json = json.loads(response.content)
            print(response_json)
            if 'result' in response_json:
                got_response = json.loads(response.content)
                with open('response_from_tg.json', 'w') as response_file:
                    json.dump(got_response, response_file)
                with open('last_post.json', 'w') as last_post_file:
                    json.dump({'date': now.strftime("%d-%m-%Y")}, last_post_file)
    else:
        new_message = render_all_message(purpose='edit')
        last_message = {}
        with open('response_from_tg.json', 'r') as tg_file:
            last_message = json.load(tg_file)
        if 'result' in last_message and new_message.replace('_', '')[:-1] != last_message['result']['text']:
            print("Posted, but changed.")
            print(new_message)
            response = requests.get('https://api.telegram.org/bot' + str(token) + '/editMessageText?parse_mode=Markdown&chat_id=' + str(choosen_chat_id) + "&message_id=" + str(last_message['result']['message_id']) + "&text=" + str(new_message))
            got_response = json.loads(response.content)
            with open('response_from_tg.json', 'w') as response_file:
                json.dump(got_response, response_file)
        else:
            print("Posted and not changed.")


# In[34]:


# with open('last_post.json', 'w') as last_post_file:
#     json.dump({'date': datetime.datetime.now().strftime("%d-%m-%Y"), 'chat_id': '-1'}, last_post_file)


# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:




